#!/usr/bin/env python3

import os
import pwd

def nix_channels(home: str, uid: int, gid: int):
    config: str = f"{home}/.nix-channels"

    with open(config, "w") as f:
        f.write(
            "https://nixos.org/channels/nixpkgs-unstable nixpkgs"
        )

    os.chown(config, uid, gid)

def nix_conf():
    with open("/etc/nix/nix.conf","a") as f:
        f.write(
            "\nexperimental-features = nix-command flakes"
        )


if __name__ == "__main__":
    users: list[str] = ["root"] + os.listdir("/home")

    nix_conf()

    for username in users:
        p = pwd.getpwnam(username)

        nix_channels(
            "/root" if username == "root" else f"/home/{username}",
            p.pw_uid,
            p.pw_gid
        )
