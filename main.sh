#!/bin/sh

su - -c "yes | (sh <(curl -L https://nixos.org/nix/install) --daemon)"
su - -c "nix-env -iA nixpkgs.python310"
su - -c "$(dirname $0)/main.py"
